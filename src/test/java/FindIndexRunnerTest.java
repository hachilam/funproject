import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by admin on 9/20/16.
 */
public class FindIndexRunnerTest {
    FindIndexRunner findIndexRunner;
    @Before
    public void setUp() {
        findIndexRunner = new FindIndexRunner();
    }

    @Test
    public void runTestCase1() {
        int[] array = {1,2,3,8,9,4,5,6,7,8,3,4};
        int a = 3;
        int b = 4;
        assertEquals(10, findIndexRunner.findIndexOfMinLengthBetweenAB(a, b, array));
    }

    @Test
    public void runTestCase2() {
        int[] array = {1,2,3,8,3,3,5,6,7,8,3,4};
        int a = 3;
        int b = 4;
        assertEquals(10, findIndexRunner.findIndexOfMinLengthBetweenAB(a, b, array));
    }

    @Test
    public void runTestCase3() {
        int[] array = {1,2,3,4,3,3,5,6,7,8,3,4};
        int a = 3;
        int b = 4;
        assertEquals(2, findIndexRunner.findIndexOfMinLengthBetweenAB(a, b, array));
    }

    @Test(expected = Exception.class)
    public void runTestCase4() {
        int[] array = {1,2,2,3,3,3,6,7,8,9};
        int a = 3;
        int b = 4;
        assertEquals(0, findIndexRunner.findIndexOfMinLengthBetweenAB(a, b, array));
    }
}