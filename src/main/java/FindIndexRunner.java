import java.util.*;
import java.util.stream.Stream;

/**
 * Created by admin on 9/20/16.
 */
public class FindIndexRunner
{
    /**
     * find the index of an integer array where the distance between a and b is minimum where a and b are elements of
     * the array. For example:
     *
     * minIndex(3, 4, [1,2,3,8,9,4,5,6,7,8,3,4] = 10
     * minIndex(3, 4, [1,2,3,8,3,3,5,6,7,8,3,4] = 10
     *
     * @param a
     * @param b
     * @param array
     * @return
     */
    public int findIndexOfMinLengthBetweenAB(int a, int b, int[] array) {
        // build the list of index pair of a and b
        ArrayList<Pair> indexList = new ArrayList<Pair>();

        Stack<Pair> pairStack = new Stack<Pair>();
        for(int i =0; i < array.length; i++) {
            if (array[i] == a) {
                pairStack.push(new Pair(i));
            }

            if (array[i] == b) {
                while(!pairStack.isEmpty()) {
                    Pair indexPair = pairStack.pop();
                    indexPair.b = i;
                    indexList.add(indexPair);
                }
            }
        }

        // find the min in the ArrayList pair
        Pair minPair = indexList.stream().min((X, Y)-> {
            int lenghtOfX = X.b - X.a;
            int lenghtOfY = Y.b - Y.a;
            if (lenghtOfX == lenghtOfY) {
                return 0;
            } else if (lenghtOfX > lenghtOfY) {
                return 1;
            } else {
                return -1;
            }
        }).get();

        return minPair.a;
    }
}
