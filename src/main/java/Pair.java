/**
 * Created by admin on 9/20/16.
 */
public class Pair {
    public int a;
    public int b;
    public Pair(int a) {
        this.a = a;
    }
    public Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }
}
